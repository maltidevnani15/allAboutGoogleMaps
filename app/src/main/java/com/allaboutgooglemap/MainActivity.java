package com.allaboutgooglemap;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Location;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.allaboutgooglemap.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
private ActivityMainBinding activityMainBinding;
    AddressResultReceiver mResultReceiver;
    boolean fetchAddress;
    int fetchType = Constants.USE_ADDRESS_LOCATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       activityMainBinding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        activityMainBinding.radioGroup.setOnCheckedChangeListener(this);
        activityMainBinding.submit.setOnClickListener(this);
        mResultReceiver = new AddressResultReceiver(null);
    }
    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId){
            case R.id.geoCodingRb:
                activityMainBinding.resulte.setText("");
                activityMainBinding.geoCodingll.setVisibility(View.VISIBLE);
                activityMainBinding.reverseGeoCodinget.setVisibility(View.GONE);
                break;
            case R.id.reverseGeoRb:
                activityMainBinding.resulte.setText("");
                activityMainBinding.geoCodingll.setVisibility(View.GONE);
                activityMainBinding.reverseGeoCodinget.setVisibility(View.VISIBLE);
                break;
            case R.id.cluster:
                Intent i=new Intent(this,ClusterActivity.class);
                startActivity(i);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                if(activityMainBinding.geoCodingll.getVisibility()==View.VISIBLE){
                    fetchType = Constants.USE_ADDRESS_LOCATION;
                    validateLatLang();
                }else{
                    fetchType = Constants.USE_ADDRESS_NAME;
                    validateAddress();
                }
                break;
        }

    }

    private void validateAddress() {
        if(!TextUtils.isEmpty(activityMainBinding.reverseGeoCodinget.getText().toString())){
            Intent intent = new Intent(this, GeoService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.FETCH_TYPE_EXTRA, fetchType);
            intent.putExtra(Constants.LOCATION_NAME_DATA_EXTRA, activityMainBinding.reverseGeoCodinget.getText().toString());
            Log.e("Service", "Starting Service");
            startService(intent);
        }else{
            Toast.makeText(this,"Enter address",Toast.LENGTH_SHORT).show();
        }

    }

    private void validateLatLang() {
        if(!TextUtils.isEmpty(activityMainBinding.latitude.getText().toString())){
            if(!TextUtils.isEmpty(activityMainBinding.longitude.getText().toString())){
                final Double lat= Double.parseDouble(activityMainBinding.latitude.getText().toString());
                final Double lang=Double.parseDouble(activityMainBinding.longitude.getText().toString());

                Intent intent = new Intent(this, GeoService.class);
                intent.putExtra(Constants.RECEIVER, mResultReceiver);
                intent.putExtra(Constants.FETCH_TYPE_EXTRA, fetchType);
                intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, lat);
                intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA,lang);
                Log.e("Service", "Starting Service");
                startService(intent);
            }else{
                Toast.makeText(this,"Enter longitude",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this,"Enter latitude",Toast.LENGTH_SHORT).show();
        }


    }
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        activityMainBinding.resulte.setText("Latitude: " + address.getLatitude() + "\n" +
                                "Longitude: " + address.getLongitude() + "\n" +
                                "Address: " + address.getSubLocality()+" "+address.getSubAdminArea()+" "+address.getPostalCode()
                        +" "+address.getCountryName());
                    }
                });
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        activityMainBinding.resulte.setText(resultData.getString(Constants.RESULT_DATA_KEY));
                    }
                });
            }
        }
    }
}
