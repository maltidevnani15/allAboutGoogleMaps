package com.allaboutgooglemap;

/**
 * Created by Malti on 3/4/2018.
 */

public class Constants {
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;

    public static final int USE_ADDRESS_NAME = 1;
    public static final int USE_ADDRESS_LOCATION = 2;

    public static final String RECEIVER =  ".RECEIVER";
    public static final String RESULT_DATA_KEY =".RESULT_DATA_KEY";
    public static final String RESULT_ADDRESS =".RESULT_ADDRESS";
    public static final String LOCATION_LATITUDE_DATA_EXTRA = ".LOCATION_LATITUDE_DATA_EXTRA";
    public static final String LOCATION_LONGITUDE_DATA_EXTRA = ".LOCATION_LONGITUDE_DATA_EXTRA";
    public static final String LOCATION_NAME_DATA_EXTRA = ".LOCATION_NAME_DATA_EXTRA";
    public static final String FETCH_TYPE_EXTRA = ".FETCH_TYPE_EXTRA";
}
