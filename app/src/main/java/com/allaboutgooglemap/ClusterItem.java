package com.allaboutgooglemap;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Malti on 3/10/2018.
 */

public  class ClusterItem implements com.google.maps.android.clustering.ClusterItem {
    private final LatLng mPosition;
    private  String mTitle ;
    private String mSnippet;

    public ClusterItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public ClusterItem(double lat, double lng, String title, String snippet) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }


}
